FROM centos
MAINTAINER Joseph Jones
RUN yum install -y rsync
VOLUME [ "/mirrors" ]
RUN adduser rsync-data
USER rsync-data
WORKDIR /mirrors
CMD [ "/usr/bin/rsync", \
      "-rtlvH", \
      "--delete-after", \
      "--delay-updates", \
      "--safe-links", \
      "rsync://mirror.es.its.nyu.edu/archlinux", \
      "./archlinux" \
    ]
