# ArchLinux Mirror containorized
 * 2 images
   * rsync image that runs an exits after completion
   * lighttpd image that starts up a lighttpd service in the foreground
 * 3 containers
   * mirror-volume: non running container that has a persistent volume to be shared
   * mirror-rsync: runs rsync and stores data in mirror-volume's volume then exits
   * mirror-service: runs lighttpd to serve mirror on port 8080. mounts mirror-volume's volume  
 
# Usage
```
 docker create -v /mirrors --name mirror-volume josephjones/archlinux-rsync /bin/true
 docker run --rm --volumes-from mirror-volume josephjones/archlinux-rsync # best to place in crontab
 docker run -d --volumes-from mirror-volume -v /var/log/lighttpd:/var/log/lighttpd -p 8080:8080 --name mirror-service josephjones/lighttpd  
```

# TODO
 * logging isn't working right now becuase of permissions
